---
title: Sobre este blog
subtitle: O porqué querrías leer esto 
comments: false
---

Mi nombre es Francisco Manuel Alexander Bueno Pérez, así todo él, largo. La verdad es que no descubrí que tenía los dos primeros hasta que tenía 4 años. Todo el mundo me llamaba por el tercero. Tambíen a esa edad empecé la escuela y descubrí nuevas cosas. Cosas que no había en mi pequeño barrio, cosas fascinantes. Recuerdo que antes de empezar cada año hojeaba los libros nuevos y me encantaba ver cosas que aún no sabía. Lo cual puede ser una desventaja porque a veces cuando llegaba en clase a ese tema ya me parecía aburrido.

Y no solo del cole recibia nuevos conocimientos, por alguna extraña razón me llevaba bien con los cachibaches tecnológicos de la casa. La radio, la tele y... bueno, eramos humildes y en un pueblo minero de los 80 tampoco había mucho más. A veces llegaban maquinitas de videojuegos. En unas navidades me regalaron una, y me encantó. Quería saber como funcionaba por dentro, más que jugar quería saber como hacía lo que hacía. Así que trasteando descubrí como poner la puntuación máxima en el juego  y también rompí los botones. Adios maquinita.... 



Decadas después he crecido mucho, sobre todo a lo ancho. Pero lo que no ha cambiado es mis ganas de aprender nuevas cosas. Dentro de mi sigue existiendo esta necesidad y ahora entiendo que compartirla puede ser útil para la comunidad. 
Aprender es un camino lleno de dificultades, que depende mucho de cada persona. Cada una tiene sus circunstancias que le pueden ayudar o no a la hora de adquirir nuevos conocimientos. 

Por eso he creado este blog, para mostraros como voy aprendiendo nuevas herramientas o conceptos. Mi idea es que no sean artículos largos, si no cortitos donde os cuente las dificultades que me encuentro y como las supero; las cosas extrañas que me pasan (que son muchas) a la hora de cacharrear. Los programas que me van gustando, las novedades científicas que leo. Pequeños trocitos de mi camino que me gustaría que vosotres también las tuvierais. Y que no sea algo estático, este blog puede ir cambiando, adaptandose a lo sea más necesario.

Espero que disfruteis del contenido. 

Un abrazo,
FMABP


