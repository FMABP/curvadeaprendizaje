---
date: 2020-12-03
title: Angel Martín en Home Assistant
tags: ["Home assistant","youtube"]
---
En el [Home Assistant](https://www.home-assistant.io/) que ando trasteando he podido crear un mini sistema para reproducir el informativo matinal de Ángel martín en una televisión lg.

Lo primero que hice fue crear un sensor con la plataforma youtube en el fichero de configuration.yaml.
```
- platform: youtube
  channel_id: UCGUc7rf0fbEzrGb7kkmu6zw
```
Luego este script que leerá la url del vídeo para extrael el id que es lo que tenemos que pasar a la entidad de la tele para reproducirlo.

```
angel_martin:
  alias: AngelMartin
  sequence:
  - choose:
    - conditions:
      - condition: template
        value_template: '{{ ''Informativo matinal express'' in states(''sensor.solocomedia'')
          }}'
      sequence:
      - service: webostv.command
        data:
          entity_id: media_player.lg_webos_smart_tv_2
          command: system.launcher/launch
          payload:
            id: youtube.leanback.v4
            contentId: '{{ state_attr(''sensor.solocomedia'',''url'').split("?v=")[-1]
              }}'
    default: []
  mode: single
​

```
Y de aqui ya se puede usar este script para añadirlo automatizacionos o exponerlo como una bombilla en emulated hue y que alexa lo encienda o no.

Ya iré aprendiendo más cosillas.


