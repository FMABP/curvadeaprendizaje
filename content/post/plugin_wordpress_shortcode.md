---
date: 2021-01-11
title: Plugins en Wordpress, shortcodes
tags: ["Wordpress","plugins","shortcode"]
---

Siguiendo con el desarrollo del plugin, he decidido que se pueda usar insertando un shortcode en un post o página. Así deja el código más limpio.

He encontrado buena información [aquí](https://kinsta.com/es/blog/codigos-cortos-wordpress/)

Estoy desarrollando con el editor EMACS, tal vez sería útil crear un comando que cree los directorios y archivos mínimos para crear un plugin en wordpress. Pero dejaré esto para más adelante.
