---
date: 2021-02-02
title: Comprimiendo pdf con imágenes en linux
tags: ["Linux","pdf","imagenes"]
---

Ante una dificultad administrativa, hoy me ha tocado aprender como bajar el tamaño de pdfs para que ocupen menos. Indagando he encontrado el siguiente código. El cuàl te creará un archivo nuevo de menor tamaño.


`
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dDownsampleColorImages=true -dColorImageResolution=20 -dNOPAUSE  -dBATCH  -sOutputFile=archivonuevo.pdf archivooriginal.pdf
`

El parámetro dColorImageResolution sirve para regular cuanto se va a reducir el tamaño. Cuanto más pequeño sea, más se reducira. A cambio de esa reducción se verá peor. Asi que hay que equilibrar en función de lo que necesites.
