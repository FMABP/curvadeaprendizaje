---
tags: ["viajes","ciencia"]
date: '2020-12-10'
title: 'Cables y contrapesos'
subtitle: 'O sobre el ayudar y el compartir'
---

<img src="https://pixelfed.social/storage/m/_v2/2784/83a90637a-595111/ZmEVxJa1jDrV/djdbOiNRhufGmrnRparf8yFWfQOybpjbs4f0Kf07.jpeg" alt="Contraapesos de catenarias" style="width:400px;border-radius: 25px;background:#f08d08;padding: 5px;" >

En un lugar de la Mancha, mientras viajaba en tren, me fijé en este poste. La maraña de cables que le colgaban no me llamó tanto la atención como esas dos columnas de piedras que le colgaban, cual tiras de chorizo.

¿Qué son?¿Para qué sirven? 

Le planteé estas preguntas a mi pareja y ella ideó una hipótesis. Necesitábamos comprobar si era acertada.

Una de las maravillas de nuestra era es internet, que, para bien o para mal, conecta a personas las cuales antes no se hubieran conocido jamás.
Así que usè esta tecnología para el bien y apaciguar, así, las dudas.

Colgué la foto en un nodo de [Pixelfed](https://pixelfed.social/p/FMA/242642862452379648) y la compartí en un 
[tweet](https://mobile.twitter.com/nUrTeS_XLII/status/1337028740111605768) 

Y no tuve que esperar mucho tiempo para que esa necesidad humana de colaborar y ayudar hiciera su aparición. 

Gracias a elles supe que estos contrapesos mantienen los cables con la tensión adecuada sin necesidad de que una persona venga cada poco rato a ajustar todo. Las explicaciones que recibí son muy buenas, os recomiendo que las leais.

También me dejaron el siguiente enlace a [Catenaria (ferrocarril) - Wikipedia, la enciclopedia libre](https://es.m.wikipedia.org/wiki/Catenaria_\(ferrocarril\)) 

Os dejo con la imagen completa de la escena:

![Estaciòn de tren en La Mancha](https://pixelfed.social/storage/m/_v2/2784/83a90637a-595111/zCBPhUy4yGHd/C3hO5lv6GQyUzJtfervQzDOvDv05Oec3KfJXGOCL.jpeg)

---

Y sí, mi pareja estaba en lo cierto, ^-^ !!!


