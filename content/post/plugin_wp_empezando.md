---
date: 2021-01-06
title: Plugins en Wordpress, empezando
tags: ["Wordpress","plugins"]
---

En mi trabajo actual llevo el mantenimiento de la web del [departamento](https://economics.mdx.ac.uk/department/). Está construida con wordpress y, hasta ahora, usaba plugins desarrollados por terceras personas.

Si tenía que hacer algo específico los modificaba y ya está. Pero ahora quiero ir un paso más allá, quiero hacer mis propios plugins.

He encontrado un  [tutorial](https://raiolanetworks.es/blog/crear-plugin-wordpress/#conocimientos_basicos_todo_lo_que_necesitas_saber_antes_de_empezar_a_crear_un_plugin_para_wordpress) que tiene buena pinta. Partiendo de él quiero hacer uno para mostrar una pequeña animación de imágenes y palabras, las cuales se configurarían en la pantalla de administración.

Ya iré contando a medida que desarrolle algo.
