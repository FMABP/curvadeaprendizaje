function giveColor(nameDiv){
    let eTitle=document.getElementById(nameDiv);
    if (eTitle != null && eTitle.innerHTML == "Curva de Aprendizaje") {
        let eTitleArray=eTitle.innerHTML.split('');
        let eTitleNew='';
        for (let i=0;i<eTitleArray.length;i++){
            if (i<3) {
                eTitleNew=eTitleNew+"<span class='titleColor'>"+eTitleArray[i]+"</span>";
            }else if(eTitleArray[i]=="A"){
                eTitleNew=eTitleNew+"<span class='titleColor'>"+eTitleArray[i]+"</span>";
            }else if(eTitleArray[i]=="r"){
                eTitleNew=eTitleNew+"<span class='titleColor'>"+eTitleArray[i]+"</span>";
            }else {
                eTitleNew=eTitleNew+eTitleArray[i];
            }
        }
        eTitle.innerHTML=eTitleNew;
    }
    return true;
}
function loadInitTitleColor(){
    giveColor('h1Title');
    giveColor('aTitle');
}
window.onload=loadInitTitleColor;